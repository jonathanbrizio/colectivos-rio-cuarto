/* -----[ Variables ]----- */
var bus = require('./app');
var express = require('express');
var app = express();

// Call to the main module
bus.main();

// Main route with alert about maintenance website
app.get('/', function(request, response) {
  response.send('¡Sitio web en construcción!');
});