/***********************************************************************
  
  Author: Jonathan Brizio
  Software Developer on Centidev Technologies
  <jonathanbrizio@gmail.com>
  http://centidev.com/
  Version 0.0.1 (Alpha) - January 2015

***********************************************************************/

exports.main = function() {
 
  /* -----[ Libraries ]----- */

  var Twitter = require('twit');
  var request = require('request');
  var config = require('./config');
  var mysql = require('mysql');
  var moment = require('moment');
  var Chance = require('chance');

  /* -----[ Configurations ]----- */

  // Set lang to Spanish for manipulate dates after
  moment.locale('es');

  // Set variables of connection with the MySQL DB
  var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'colectivos',
    port: 3306
  });

  // Initialize the connection
  connection.connect(function(err) {
    if (err) {
      console.error("¡Connection error!");
      return;
    }
    console.log('Open connection and established with thread ID ' + connection.threadId);
  });

  // Create Twitter object with the configurations
  var twitter = new Twitter(config);

  // Instantiate Chance so it can be used
  var chance = new Chance();

  /* -----[ Twitter Procedures ]----- */

  // Filter @mentions
  var stream = twitter.stream('statuses/filter', { track: config.twitter_account });

  // Conect with the Twitter API
  stream.on('connect', function (response) {
    console.log("Conection with Twitter successfully!")
  });

  // Listen tweets
  stream.on("connected", function (response) {
    console.log("Waiting Tweets to response!")
  });

  // Upon receiving a new @mention execute the function
  stream.on('tweet', function (tweet) {
    
    // Declare the const with the initiation and the end of bus service
    const initiationService = moment('05:30:00','HH:mm:ss');
    const endService = moment('22:35:00','HH:mm:ss');
    
    // Hour validation
    var now = moment();
    try {
      // If is bussiness day
      if ((moment().day() >= 1) && (moment().day() <= 5)) {
        if((now >= initiationService) && (now <= endService)) {
          handleTweet(tweet);
        } else {
          maintenanceTweet(tweet);
        }
      } else {
        // If Weekend
        weekendAlert(tweet);
      }
    } catch (err) {
      console.log("An error has occurred while handling this message: " + err);
    }
  });

  /**
   *
   *  Name: randomAlert
   *  Information: Function that return random message with alerts  
   *  
   */

  function randomAlert() {
    var option = chance.integer({min: 1, max: 5});
    switch(option) {
      case 1:
        var message = "En este momento todas las líneas se encuentran fuera del horario de servicio.";
        break;
      case 2:
        var message = "Le informamos que todas las líneas se encuentran fuera del horario de servicio.";
        break;
      case 3:
        var message = "Actualmente todas las líneas se encuentran fuera del horario de servicio.";
        break;
      case 4:
        var message = "Lamentablemente todas las líneas se encuentran fuera del horario de servicio.";
        break;
      case 5:
        var message = "Parece ser que todas las líneas se encuentran fuera del horario de servicio.";
        break;
    }
    return message;
  }

  /**
   *
   *  Name: maintenanceTweet
   *  Params: tweet - Array with all data of the Tweet, line - number of bus line, stop - name of bus stop,
   *  horary - time when the bus arrives
   *  Information: Function that publish the tweet on response of the answer received
   *  
   */

  function maintenanceTweet(tweet) {
    // Get the username for create the response
    var status = "@" + tweet.user.screen_name;
    // Manipulate the dates with MomentJS
    var start = moment();
    var greeting = getGreetingTime(start);
    var alert = randomAlert();
    status += greeting + alert;
    twitter.post('statuses/update', { status: status, in_reply_to_status_id: tweet.id_str }, function (err, data, response) {
      if (err) {
        console.log(err);
      } else {
        console.log("Maintenance response published successfully!");
      }
    });
  }

  /**
   *
   *  Name: weekendAlert
   *  Params: tweet - Array with all data of the Tweet
   *  Information: Function that publish a tweet with alert to user about the user on weekends
   *  
   */

  function weekendAlert(tweet) {
    // Get the username for create the response
    var status = "@" + tweet.user.screen_name;
    // Manipulate the dates with MomentJS
    var start = moment();
    var greeting = getGreetingTime(start);
    status += greeting + " Momentamente nuestro servicio solo se encuentra disponible para la consulta de los horarios de Lunes a Viernes.";
    twitter.post('statuses/update', { status: status, in_reply_to_status_id: tweet.id_str }, function (err, data, response) {
      if (err) {
        console.log(err);
      } else {
        console.log("Weekend response published successfully!");
      }
    });
  }

  /**
   *
   *  Name: respondToTweetInvalid
   *  Params: tweet - Array with all data of the Tweet
   *  Information: Function that create a message with the response of the invalid tweet
   *
   */

  function respondToTweetInvalid(tweet) {
    // Get the username for create the response
    var status = "@" + tweet.user.screen_name;
    status += " Tu consulta no es válida.";
    twitter.post('statuses/update', { status: status, in_reply_to_status_id: tweet.id_str }, function (err, data, response) {
      if (err) {
        console.log(err);
      } else {
        console.log("Response to invalid tweet published successfully!");
      }
    });
  }

  /**
   *
   *  Name: respondToTweetWithoutResults
   *  Params: tweet - Array with all data of the Tweet
   *  Information: Function that create a message with the response of the invalid tweet
   *
   */

  function respondToTweetWithoutResults(tweet) {
    // Get the username for create the response
    var status = "@" + tweet.user.screen_name;
    status += " No hemos encontrado resultados en nuestros registros para esa línea de colectivos.";
    twitter.post('statuses/update', { status: status, in_reply_to_status_id: tweet.id_str }, function (err, data, response) {
      if (err) {
        console.log(err);
      } else {
        console.log("No results for query executed!");
      }
    });
  }

  /**
   *
   *  Name: findMatch
   *  Params: tweet - Array with all data of the Tweet, line - number of bus line, stop - name of bus stop
   *  Information: Function that execute the SQL in the MySQL Database
   *  with the params provide before in the call to action.
   *
   */

  function findMatch(tweet, line, stop) {
    // Execute the query in the MySQL DB
    var sql = "SELECT lineas.nombre AS `linea`, paradas.nombre AS `parada`, horarios.hora AS `horario` FROM horarios INNER JOIN paradas ON horarios.paradas_id = paradas.id INNER JOIN lineas ON horarios.lineas_id = lineas.id WHERE (lineas.nombre = " + line + ") AND (CURTIME() <= horarios.hora) AND (paradas.nombre LIKE '%" + stop + "%') ORDER BY horarios.hora ASC LIMIT 1";
    connection.query(sql, function(err, rows, fields) {
      // Have results?
      if (rows.length > 0) {
        respondToTweet(tweet, rows[0].linea, rows[0].parada, rows[0].horario);
      } else {
        respondToTweetWithoutResults(tweet);
      }
    });
  }

  /**
   *
   *  Name: isNumber
   *  Params: n - Character
   *  Information: Function that return true o false depending of the value of n
   *
   */

  function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  /**
   *
   *  Name: handleTweet
   *  Params: tweet - Array with all data of the Tweet
   *  Information: Function that manipulates and takes
   *  appropriate action based on the Tweet received.
   *
   */

  function handleTweet(tweet) {
    // Storage and manipulate the Tweet received how string. Structure: @mention LINE STOP COLOR
    var text = tweet.text;
    var arrayQuery = text.split(" ");
    var line = arrayQuery[1];
    var stop = arrayQuery[2];
    var color = arrayQuery[3];

    // Initial validation of Tweet
    if ((isNumber(line)) && (stop != null)) {
      findMatch(tweet, line, stop);
    } else {
      respondToTweetInvalid(tweet);
    }
  }

  /**
   *
   *  Name: getGreetingTime
   *  Information: Function that return the correct greetings based
   *  on the current time of executed this.
   *
   */

  function getGreetingTime () {
    var greeting = null;
    var currentHour = moment().hour();
    switch(currentHour) {
      case 0:
        greeting = " ¡Buenas noches! ";
        break;
      case 1:
        greeting = " ¡Buenas noches! ";
        break;
      case 2:
        greeting = " ¡Buenas noches! ";
        break;
      case 4:
        greeting = " ¡Buenas noches! ";
        break;
      case 5:
        greeting = " ¡Buenas noches! ";
        break;
      case 6:
        greeting = " ¡Buenos días! ";
        break;
      case 7:
        greeting = " ¡Buenos días! ";
        break;
      case 8:
        greeting = " ¡Buenos días! ";
        break;
      case 9:
        greeting = " ¡Buenos días! ";
        break;
      case 10:
        greeting = " ¡Buenos días! ";
        break;
      case 11:
        greeting = " ¡Buenos días! ";
        break;
      case 12:
        greeting = " ¡Buenos días! ";
        break;
      case 13:
        greeting = " ¡Buenas tardes! ";
        break;
      case 14:
        greeting = " ¡Buenas tardes! ";
        break;
      case 15:
        greeting = " ¡Buenas tardes! ";
        break;
      case 16:
        greeting = " ¡Buenas tardes! ";
        break;
      case 17:
        greeting = " ¡Buenas tardes! ";
        break;
      case 18:
        greeting = " ¡Buenas tardes! ";
        break;
      case 19:
        greeting = " ¡Buenas tardes! ";
        break;
      case 20:
        greeting = " ¡Buenas noches! ";
        break;
      case 21:
        greeting = " ¡Buenas noches! ";
        break;
      case 22:
        greeting = " ¡Buenas noches! ";
        break;
      case 23:
        greeting = " ¡Buenas noches! ";
        break;
    }
    return greeting;
  }

  /**
   *
   *  Name: respondToTweet
   *  Params: tweet - Array with all data of the Tweet, line - number of bus line, stop - name of bus stop,
   *  horary - time when the bus arrives
   *  Information: Function that publish the tweet on response of the answer received
   *  
   */

  function respondToTweet(tweet, line, stop, horary) {
    // Get the username for create the response
    var status = "@" + tweet.user.screen_name;
    // Manipulate the dates with MomentJS
    var start = moment();
    var end   = moment(horary,'HH:mm:ss'); 
    var minutes = end.from(start);
    var greeting = getGreetingTime();
    status += greeting + "La línea " + line + " pasa " + minutes + " apróximadamente por la parada ubicada en " + stop + ".";
    twitter.post('statuses/update', { status: status, in_reply_to_status_id: tweet.id_str }, function (err, data, response) {
      if (err) {
        console.log("Error to respond to tweet!");
      } else {
        console.log("Response published successfully!");
      }
    });
  }

}