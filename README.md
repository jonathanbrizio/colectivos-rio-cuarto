# Colectivos Rio Cuarto

The purpose of this development is *purely and exclusively * to provide the citizens of Rio Cuarto a tool that allows them to get in seconds the approximate time waiting at the stop based on bus line to indicate in the message sent via tweet mention our [account on Twitter].

Tweet example:

> @colectivos_rio4 **7** **Plaza**

Explanation: Where *"7"* is the number of line, and *"Plaza"* is the bus stop location. In a few seconds your received a response with the time waiting at the stop.

### Tech

  * [Node.js] - Platform to built scalable network applications
  * [npm] - Package manajer for JavaScript
  * [Twitter API]

### Version
0.0.1 Alpha (January 2015)

### Collaboration

To collaborate with this project sendme e-mail to: [jonathanbrizio@gmail.com]

License
----

MIT

**Free Software, Hell Yeah!**

[Node.js]:http://nodejs.org/
[npm]:https://www.npmjs.com/
[Twitter API]:https://dev.twitter.com/
[jonathanbrizio@gmail.com]:mailto:jonathanbrizio@gmail.com
[account on Twitter]:https://twitter.com/colectivos_rio4